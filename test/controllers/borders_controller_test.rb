require 'test_helper'

class BordersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get borders_index_url
    assert_response :success
  end

  test "should get show" do
    get borders_show_url
    assert_response :success
  end

  test "should get edit" do
    get borders_edit_url
    assert_response :success
  end

  test "should get new" do
    get borders_new_url
    assert_response :success
  end

end
