Rails.application.routes.draw do
  resources :tasks

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
  resources :borders do
    collection do
      get 'about'
      get 'howto'
    end
  end
  root 'borders#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
