class BordersController < ApplicationController
  before_action :authenticate_user!, only: [:new]
  before_action :set_border, only: [:show, :edit, :update, :destroy]
  def index
    @border = Border.all
  end

  def show
  end

  def edit
  end

  def new
    @border = Border.new
  end

  def create
    @border = Border.new(border_params)
    @border.user_id = current_user.id
    if @border.save
      redirect_to root_path, notice: '作成できました'
    else
      render :new, alert: '作成できませんでした'
    end
  end

  def update
    if @border.update(border_params)
      redirect_to @border, notice: '更新できました'
    else
      render :show, alert: '更新できませんでした'
    end
  end

  def destroy
    if @border.destroy
      redirect_to root_path, notice: '削除しました'
    else
      redirect_to root_path, alert: '削除に失敗しました'
    end
  end

  def about
  end

  private
    def set_border
      @border = Border.find(params[:id])
    end

    def border_params
      params.require(:border).permit(:memo, :subject, :content, :deadline)
    end
end
