class TasksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_task, only: [:show, :edit, :update, :destroy]
  before_action :validate_user, only: [:show, :edit, :update, :destroy]

  def index
    @task = current_user.tasks.all
  end

  def show
  end

  def edit
  end

  def new
    @task = Task.new
  end

  def create
    @task = Task.new(task_params)
    @task.user_id = current_user.id
    if @task.save
      redirect_to root_path, notice: '作成できました'
    else
      render :new, alert: '作成できませんでした'
    end
  end

  def update
    if @task.update(task_params)
      redirect_to root_path, notice: '更新できました'
    else
      render :show, alert: '更新できませんでした'
    end
  end

  def destroy
    if @task.destroy
      redirect_to root_path, notice: '削除しました'
    else
      redirect_to root_path, alert: '削除に失敗しました'
    end
  end

  def about
  end

  private
    def set_task
      @task = Task.find(params[:id])
    end

    def task_params
      params.require(:task).permit(:title, :body)
    end

    def validate_user
      if @task.user != current_user
        redirect_to root_path, alert: "自分のタスクではありません"
      end
    end
end
